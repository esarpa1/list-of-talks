# List of Talks
- **Jan 23, 2025**: Lead Project Report "Cosmology from BAO in 2-point Galaxy Clustering in DR1 Data" at "annual Euclid Galaxy Clustering Meetig", Minich, Germany
- **Jan 22, 2025**: presentation of the work on "Constrain Interloper contamination inthe Early Euclid wide field" at "annual Euclid Galaxy Clustering Meetig", Minich, Germany
- **Jan 15, 2025**: talk at the "BAO Extraction in the Euclid Era " at "OATS Seminats" at The Astronomical Observatory of Trieste, Trieste, Italy
- **Dec 19, 2024**: Invited talk "II ICSC Spoke 3 National meeting" at CNR, Bologna, Italy
- **Oct 1, 2024**: Invited talk at "Euclid Theory Working Group meeting in Madrid" at Institute for Theoretical Physics UAM - CSIC, Madrid, Spain
- **Jun 7, 2024**: Invited Lecturer for "Precision Cosmology: and introduction" at Universitá di Trieste, Italy
- **Jun 3, 2024**: Project status Report at the Euclid Consortium Galaxy Clustering Science working group telecon
- **May 6, 2024**: Talk at PNRR- High Performance Computing Center Spoke3  "Make Cosmological Analysis Accessible:
A Unified Pipeline for IV Spectroscopic Survey BAO Analysis", Department of Mathematics, Portoferraio, Isola d'Elba, Italy
- **February 19, 2024**: invited lecturer for  "Optimal Transport in Cosmology and the
Evolution of Large-Scale Structures", Department of Mathematics, University of Padova, Padova, Italy
- **February 16, 2024**: seminar at IFPU journal club The galaxy evolution & Cosmic-web connection”, IFPU, Trieste, Italy
- **February 2, 202**4: WP-NL status report at Euclid GC-SWG Annual meeting, splinter session, Marseille, France
- **Jan 11, 2024**: Local Seminar at LSS-IFPU meeting on “The theory behind standard reconstruction”, IFPU, Trieste, Italy
- **December 21, 2023**: Invited Talk at on Effect of interlopers on 2PCF estimates, Euclid Forecast, UAB, Barcelona, Spain
- **October 10, 2023**: Talk at technical meeting Spoke3 for PNRR-HPC program, University of Trieste,Trieste
- **October 3, 2023**: Invited speaker at  Euclid IST:NL Team Meeting 2023 , UAB,Barcelona,Spain
- **September 21 2023**: Seminar at HPC-Spoke3 Local Meeting on Multidimensional optimisers in Cosmology, IFPU, Trieste
- **September 13 2023**: Invited Lecturer at COSMOSTAT2023, Astrophysical Observatory at Monte Pennar, University of Padova, Italy, University of Padova, https://agenda.infn.it/event/34608/
- **April 23, 2023**: Invited speaker UCLA Tuesday Lunch Talks, UCLA, Los Angeles, US
- **March 8 2023**: Talks at the The Cosmic Web: Connecting Galaxies to Cosmology at High and Low Redshift program (Invited), KITP institute for theoretical physics, Santa Barbara, US 
- **February 23 2023**: Invited Plenary talk at Euclid Consortium Galaxy Clustering Meeting - BAO reconstruction, University of Milan, Milan
- **November 30 2022**: Euclid-frace meeting “BAO’s extraction techniques: comparison and optimisation. Euclid DR1 forecast,”As co-leader of the WP Non-linear, Euclid France Symposium, IAP, Paris 
- **November 16 2022**: (Invited) Talk at Euclid HOS & NL KP meeting 2022, IFPU-Trieste, Italy
- **November 3 2022**: Invited speaker at CosmoMib seminar, “Following galaxies across the cosmic web: the way of reconstruction”  University di Milano Bicocca, Milan,Italy 
- **October 27, 2022**:Invited speaker at UniGe Cosmology group meeting, “Following galaxies across the cosmic web: the way of reconstruction”  University of Genova, Genova, Italy 
-  **May 3 2022**: DESI France meeting, “The extended Fast Action Minimisation method: BAO reconstruction in the SDSS-DR12 galaxy sample” Marseille 
- **2 March 2022**:  Talk for RENOIR meeting, CPPM, Marseille.
- **February 29, 2022**: Invited speaker at National Centre for the Nuclear Research monthly 
seminar, NCBJ, Warsaw. 
- **September 30, 2021** : Invited speakerat ICAP monthly seminar, IAP, Paris. 
- J**uly 9, 2021**: Invited speakerat BIPAC journal club, Oxford University, Oxford. 
- **May 6, 2021**: Invited speaker at AP galaxy circle, IAP, Paris. 
- **March 18, 2021**: Invited speaker weekly seminar at ICCUB, ICCUB, Barcelona. 
- **February 11, 2021**: Invited speaker monthly seminar at Univr, Universita degli studi di Verona, Verona. 
- **December 17, 2020**: Talk at the Euclid Non-linear working package
- **May 18, 2020**: Virtual talk at "Atelier Dark Energy", CPPM, Marseille. 
- **May 13, 2020**: Weekly virtual UNIPD journal club (invited), University of Padova, Padova. 
- **February 3, 2020**: Talk for the Euclid Galaxy Cluster Science Working Group, IAP, Paris 
- **January 27, 2020**: Talk (invited) for The cosmic web in the local Universe, Lorentz center, 
Leiden 
- **Jun 4-7, 2019**: Talk for the Euclid Galaxy Clustering Science Working Group, Euclid Consortium Annual Meeting, Helsinki. 
- **May 24, 2019**: Invited speaker for ECU meeting at Unige, Geneve. 
- **April 4, 2019** : Talk at “UniVersum”, Milan. 
-  **November 5, 201**8: Weekly journal club (invited), ICTP-SAIFR. 
- **October 24, 2018**: Weekly cosmology seminar (invited), Universidade de São Paulo, São Paulo.
- **October 8, 2018**: Talk at Cosmolunch (invited), Princeton University.
- **October 1, 2018**: Talk at the DESI lunch meeting (invited), LBNL, UC Berkeley. 
- **September 9**, 2018: Talk at the Northern Skies Cosmic Flows workshop , Laboratoire d’Astrophysique de Marseille. 
- **Jun 13**, 2018: Talk at the Euclid Likelihood working package, Euclid Consortium Annual Meeting, Bonn. 
- **Jun 4, 2018**: Talk for the Euclid-France Galaxy Cluster Science Working Group meeting, Laboratoire d’Astrophysique de Marseille. 
- **May 18, 2018**: CRAL weekly seminar (invited), Centre de Recherche Astrophysique de Lyon, Lyon 
- **Jan 18, 2018**: Weekly journal club (invited) , University of Padova. 

